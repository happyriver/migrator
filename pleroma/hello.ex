defmodule Mix.Tasks.Migrator.Hello do
  use Mix.Task

  @shortdoc "Test that Pleroma can run this task."
  def run(_) do
    IO.puts("Hello, World!")
  end
end
