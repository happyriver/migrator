# Soapbox Migrator

> :warning: This project is **in the beginning stages** and is not even close to complete.
It's being developed in the open.
If you'd like to help, please open an issue.

Export data from Mastodon 2.8.4 and import it into Pleroma 2.0.3.

## Disclaimer

This repo is not for the faint of heart.
If you came here wondering if you can migrate Mastodon to Pleroma, know that it will not be easy.
This project is inherently fragile because Mastodon and Pleroma are both moving targets, and they implement some things in fundamentally different ways.

Just because it worked for one site doesn't mean it will work for another.
There are a lot of places where things could go wrong.

That said, we are aiming to make it as easy as possible, in the hope it can help the Fediverse advance to the next stage.
We believe that Pleroma is the future, and that it will come in all shapes and sizes.

## How it works

- Mastodon is created with [Ruby on Rails](https://rubyonrails.org/) (Ruby), and Pleroma is created with [Phoenix](https://www.phoenixframework.org/) (Elixir).

- Both frameworks have an [ORM](https://en.wikipedia.org/wiki/Object-relational_mapping), which lets developers manage database objects directly in the code.

- This migrator contains Ruby code used by Mastodon to create an export using its ORM, and Elixir code used by Pleroma to import the data using its ORM.

- There is a JavaScript task runner using [Gulp](https://gulpjs.com/) that copies the files into the right places and runs the proper commands.

## Philosophy

The theory for this migrator is that it should be hard to export and easy to import.
It's not meant to be a new standard for interoperable Fediverse databases, it's just meant to solve the use case of Mastodon to Pleroma.
It's a one way function.

We also don't intend for this code to ever be merged upstream.
We're trying to solve the easy problem first instead of the hard one, just to prove it can work, and to keep the finish line closer.

So the goal is to make Mastodon export a format that's as close to what Pleroma needs as possible, then Pleroma will just have to read it without (hopefully) doing anything too crazy.

## Usage

You will need local repositories of Mastodon and Pleroma.
You must configure them with the needed environment and make sure they start up properly.
The database you're trying to convert should be loaded into Mastodon.

Create a `.env` file and configure the local paths to Mastodon and Pleroma:

```bash
MASTODON_PATH=/home/alex/Projects/mastodon
PLEROMA_PATH=/home/alex/Projects/pleroma
```

Install dependencies:

```bash
yarn
```

Eventually you'll be able to run these commands to migrate the database (not yet supported):

```bash
yarn masto export
yarn pleroma import
```

## Development

Now you can run commands like this:

```bash
yarn masto hello
yarn pleroma hello
yarn masto export:users
yarn pleroma import.users
```

The commands correspond to [Rake](https://github.com/ruby/rake) tasks and [Mix](https://elixirschool.com/en/lessons/basics/mix/) tasks, for Mastodon and Pleroma respectively.

- **Rake** tasks end in `.rake` and are found in `mastodon/`

- **Mix** tasks end in `.ex` and are found in `pleroma/`

The Yarn commands are shorthand for [gulp](https://gulpjs.com/) commands:

```bash
npx gulp masto --task [rakeTask]
npx gulp masto --task [mixTask]
```

Which are in turn shorthand for commands like this:

```bash
cd $MASTODON_PATH && bundle exec rake migrator:[rakeTask]

cd $PLEROMA_PATH && mix migrator.[mixTask]
```

Notice how all tasks are in the `migrator` namespace, and the Yarn command conveniently lets you drop them.
The task files still need that namespace, however.

The benefit of using Yarn is that it will automatically copy the tasks from this repo into the correct location before running them, as well as move the exported data afterwards.
