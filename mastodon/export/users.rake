# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export accounts and users'
    task :users => :environment do
      filename = 'migrator/users.txt'
      FileUtils.mkdir_p 'migrator'
      File.delete(filename) if File.exists? filename
      File.open(filename, 'a') do |file|
        Account.find_each do |a|
          u = a&.user
          file.puts({
            id: a.id,
            email: u&.email,
            password_hash: a.local? ? "bcrypt_#{u&.encrypted_password}" : nil,
            name: a.username,
            nickname: a.display_name,
            bio: a.note,
            inserted_at: nil,
            updated_at: nil,
            ap_id: nil,
            avatar: nil,
            local: a.local?,
            follower_address: nil,
            last_refreshed_at: nil,
            tags: nil,
            last_digest_emailed_at: nil,
            following_address: nil,
            keys: nil,
            banner: nil,
            background: nil,
            note_count: nil,
            follower_count: nil,
            following_count: nil,
            locked: nil,
            confirmation_pending: nil,
            password_reset_pending: nil,
            confirmation_token: nil,
            default_scope: nil,
            blocks: nil,
            domain_blocks: nil,
            mutes: nil,
            muted_reblogs: nil,
            muted_notifications: nil,
            subscribers: nil,
            deactivated: nil,
            no_rich_text: nil,
            ap_enabled: nil,
            is_moderator: u&.moderator,
            is_admin: u&.admin,
            show_role: nil,
            settings: nil,
            magic_key: nil,
            uri: a.uri,
            hide_followers_count: nil,
            hide_follows_count: nil,
            hide_followers: nil,
            hide_follows: nil,
            hide_favorites: nil,
            unread_conversation_count: nil,
            pinned_activities: nil,
            email_notifications: nil,
            mascot: nil,
            emoji: nil,
            pleroma_settings_store: nil,
            fields: nil,
            raw_fields: nil,
            discoverable: nil,
            invisible: nil,
            notification_settings: nil,
            skip_thread_containment: nil,
            also_known_as: nil,
            allow_following_move: nil,
            actor_type: nil,
            public_key: a.public_key,
            inbox: a.inbox_url,
            shared_inbox: a.shared_inbox_url,
          }.to_json)
        end
      end
    end
  end
end
