require('dotenv').config();
import { src, dest, series, parallel } from 'gulp';
import { join } from 'path';
import { spawn } from 'child_process';
import { argv } from 'yargs';

const { MASTODON_PATH, PLEROMA_PATH } = process.env;

const runCmd = (path, cmd, done) => {
  const s = spawn('bash');
  s.stdin.end(`cd "${path}" && ${cmd}`);
  s.stdout.on('data', data => console.log(data.toString()));
  s.stderr.on('data', data => console.error(data.toString()));
  s.on('error', error => console.error(error.message));
  s.on('close', code => {
    console.log(`child process exited with code ${code}`);
    done();
  });
}

const copyMastoTasks = () => src('mastodon/**')
  .pipe(dest(join(MASTODON_PATH, 'lib/tasks/migrator/')));

const copyPleromaTasks = () => src('pleroma/**')
  .pipe(dest(join(PLEROMA_PATH, 'lib/mix/tasks/migrator/')));

const copyTasks = parallel(copyMastoTasks, copyPleromaTasks);

const moveInMastoExports = () => src(join(MASTODON_PATH, 'migrator/**'))
  .pipe(dest('data/'));

const rake = done => {
  runCmd(MASTODON_PATH, `bundle exec rake migrator:${argv.task}`, done);
}

const mix = done => {
  runCmd(PLEROMA_PATH, `mix migrator.${argv.task}`, done);
}

export const masto   = series(copyMastoTasks, rake, moveInMastoExports);
export const pleroma = series(copyPleromaTasks, mix);

export default copyTasks;
